# NixOS User Group Austria Website [<img src="static/images/logo.svg" align="right" width="28">](https://nixos.at)

This is the repository for the user group's website template.

The website is generated from this template using the static site generator framework [HUGO](https://gohugo.io) and is currently hosted by [Easyname](https://easyname.at).

It is accessible at https://nixos.at.
